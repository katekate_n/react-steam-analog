import React from 'react';
import './InnerNavigation.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowsAltV } from '@fortawesome/free-solid-svg-icons';
import { faSortAmountDownAlt } from '@fortawesome/free-solid-svg-icons';
import { faSortAmountDown } from '@fortawesome/free-solid-svg-icons';

const innerNavigation = (props) => {

   const getSortingClass = (val) => {
      return val == null
         ? faArrowsAltV
         : (val == true
            ? faSortAmountDownAlt
            : faSortAmountDown);
   }

   const detActiveFilter = (val) => {
      return val != null
   }

   const onSearchInputChange = (e) => {
      props.onSearch(e.target.value);
   }

   const pageNumbers = [];

   for(let i = 1; i <= Math.ceil(props.totalPosts / props.postPerPage); i++){
      pageNumbers.push(i);
   }

   
   return (
      <div>
         <div className = "navigation">
            <div className = "navigation__search-box">
               <div className = "navigation__search-text">search:</div>
               <input className = "navigation__input" onChange={onSearchInputChange} type = "text" placeholder = "type here"/>
            </div>
            <div className = "pagination">
               <ul className = "pagination__container">
                  {pageNumbers.map(number => (
                     <li key={number} 
                        className= "pagination__numbers">
                           <a className = {props.currentPage == number ? 'activePage' : null} href="#" 
                           onClick={() => props.paginate(number)}>{number}</a>
                     </li>
                  ))}
               </ul>
            </div>
         </div>
         <div className = "filters">
            <div className = { props.sortingState["reverse"] != null ? "filter--active" : "filter" } onClick={()=> props.sortBy("reverse")}># 
               <FontAwesomeIcon icon = { getSortingClass(props.sortingState["reverse"]) }  className = "icon"/>
            </div>
            <div className = { props.sortingState["name"] != null ? "filter--active" : "filter" } onClick={() => props.sortBy("name")}>game 
               <FontAwesomeIcon icon = { getSortingClass(props.sortingState["name"]) } className = "icon" /></div>
            <div className = { props.sortingState["date"] != null ? "filter--active" : "filter" } onClick={() => props.sortBy("date")}>release <br/> date 
               <FontAwesomeIcon icon = { getSortingClass(props.sortingState["date"]) } className = "icon" /></div>
            <div className = { props.sortingState["price"] != null ? "filter--active" : "filter" } onClick={() => props.sortBy("price")}>price 
               <FontAwesomeIcon icon = { getSortingClass(props.sortingState["price"]) } className = "icon" /></div>
            <div className = { props.sortingState["reviews"] != null ? "filter--active" : "filter" } onClick={() => props.sortBy("reviews")}>reviews 
               <FontAwesomeIcon icon = { getSortingClass(props.sortingState["reviews"]) } className = "icon" />
               </div>
            <div className = { props.sortingState["owners"] != null ? "filter--active" : "filter" } onClick={() => props.sortBy("owners")}>owners 
               <FontAwesomeIcon icon = { getSortingClass(props.sortingState["owners"]) } className = "icon" /></div>
         </div>
      </div>   
   );
};

export default innerNavigation; 