import React, {useState, useEffect} from 'react';
import InnerNavigation from '../InnerNavigation/InnerNavigation';
import { Link } from 'react-router-dom';

import GameService from '../../services/gameService';
import SortingHelper from '../../helpers/sortingHelper';


const Trending = (props) => {

   const [games, setGames] = useState([]);
   const [searchResults, setSearchResults] = React.useState([]);
   const [searchText, setSearchText] = useState("");
   const [sortingState, setSortingState ] = useState( { 'reverse' : null, 'date': null, 'price': null, 'owners' : null, 'reviews' : null, 'name' : null } );

   //paging
   const [currentPage, setCurrentPage] = useState(1);
   const [postPerPage] = useState(20);
   const [activePage, setActivePage] = useState(false);
 

   const fetchGames = async () => {
      let results = await GameService.getTrending();
      setGames(results);
   };


   //on init 
   useEffect(() => {
      fetchGames();
    }, []);

    //on component init + when this changes [searchText, sortingState, games]
   useEffect(() => {

      setCurrentPage(1);

      let results = games
         .filter(x => x.name.toLowerCase().replace(/ /g, '')
            .includes(searchText.toLowerCase().replace(/ /g, '')));

      results = SortingHelper.sortResultsByState(sortingState, results);
      setSearchResults(results);

   }, [searchText, sortingState, games]);

   const onSearch = (text) => {
      setSearchText(text);
      setCurrentPage(1);
   }

   //paging
  const indexOfLastPost = currentPage * postPerPage;
  const indexOfFirtPost = indexOfLastPost - postPerPage;
  const currentPosts = searchResults.slice(indexOfFirtPost, indexOfLastPost);

  const changePage = pageNumber => {
     setCurrentPage(pageNumber);
   }

   // reset sorting 
   const resetFilters = (except) => {
      let keys = Object.getOwnPropertyNames(sortingState).filter(name => name != except);
      keys.forEach(key =>  sortingState[key] = null);
   }


   const changeSorting = (sortName) => {
      resetFilters(sortName);
      sortingState[sortName] = !sortingState[sortName]; 
      setSortingState(Object.assign({} , sortingState));
   }

   const sortBy = (sort) => {
      changeSorting(sort);
   }

   const activeNumber = () => {
      setActivePage(!activePage);
   };
   

   const showGames = currentPosts.map((game) => {
      return (
         <div className= "game-wrapper ">
            <div className= "game">
               <div className= "game__desc">{game.index}</div>
               <Link to= {{ pathname: '/game', state: game
                        }}>       
                  <div className= "game__desc game__title">
                     <img src= {game.header_image} className= "game__img" />
                     <div className= "game__name">   {game.name}</div>
                  </div>

               </Link>
               { game.release_date.coming_soon 
                  ? (<div className= "game__desc">Coming Soon</div>) 
                  : (<div className= "game__desc"> {game.release_date.date} </div>) }
               { game.is_free || game.price_overview == null 
                  ? ( <div className= "game__desc"> free</div> ) 
                  : ( <div className= "game__desc"> {game.price_overview.final / 100}<span> {game.price_overview.currency}</span> </div> )}
               <div className="game__desc">({game.positive}/{game.negative}) </div>
               <div className="game__desc"> {game.owners} </div>
            </div>
         </div>
      )
   })


   return (
         <div className="container">
            <div className="container__title">trending now on steam</div>
            <InnerNavigation 
               sortBy = {sortBy}
               postPerPage= {postPerPage} 
               totalPosts= {searchResults.length} 
               paginate= {changePage}
               activeNumber= {activeNumber}
               activePage= {activePage}
               currentPage= {currentPage}
               onSearch = {onSearch}
               sortingState = {sortingState}
            />
            <div className='games' >
               {showGames}
            </div>
         </div>
   );
};

export default Trending; 
