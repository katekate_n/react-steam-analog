import React, {useState, useEffect} from 'react';
import './UpcommingGames.scss'
import GameService from '../../services/gameService';
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


const UpcommingGames = () => {

   const[games, setGames] = useState([]);

   const fetchGames = async () => {
      var results = await GameService.getUpcomming();
      setGames(results);
   };

   useEffect(() => {
      fetchGames();
    }, []);


const getGenres = (game) => game.genres.map(x => x.description).join(" ▪ ");


  const showUpcommingGames = games.map((game) => {
     return (
        <div className="container__game">
           <div className="container__name">{game.name}</div>
            <Link to={{ pathname: '/game', state: game}}>       
               <div className="container__img"><img src={game.header_image}></img></div>
            </Link>
           <div className="container__info">
              <div className="container__details"><span className="container__details--bold">Developer:</span>{game.developers}</div>
              <div className="container__details"><span className="container__details--bold">Publisher:</span>{game.publishers}</div>
              <div className="container__details"><span className="container__details--bold">Genre:</span>{getGenres(game)}</div>
              <div className="container__details"><span className="container__details--bold">Release date:</span>{game.release_date.date}</div>
              <div className="container__details"><span className="container__details--bold">Owners:</span>{game.owners}</div>
           </div>
        </div>
     );
  });
  return (
   <div className = "container">
      <div className = "container__title">coming soon to steam near you</div>
      <div className = "container__games">
         {showUpcommingGames}
      </div>
   </div>
  )
};



export default UpcommingGames; 