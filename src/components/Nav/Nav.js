import React from 'react';
import './Nav.scss';
import Trending from '../TrendingGames/TrendingGames';
import RecentGames from '../RecentGames/RecentGames';
import UpcommingGames from '../UpcommingGames/UpcommingGames';
import Playtime from '../Playtime/Playtime';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const contentList = [<Trending/>, <RecentGames/>, <UpcommingGames/>, <Playtime/>];
const content = contentList.map(content => {
  return (
      <TabPanel>{content}</TabPanel>
  )
})

const nav = () => {

  const displayContent = (
    <Tabs defaultIndex= {0} onSelect={index => console.log(index)}>
      <TabList className="menu">
        <Tab className="menu__item">trending games</Tab>
        <Tab className="menu__item">recent games</Tab>
        <Tab className="menu__item">upcomming games</Tab>
        <Tab className="menu__item">top by playtime</Tab>
        <span className="menu__item--disabledWrapper">
          <Tab className="menu__item menu__item--disabled"> steam stats</Tab>
        </span>
      </TabList>
      <div className="content">
        {content}
      </div>
    </Tabs>
  )

 return (
   <div>
     <div className = "wrapper">
      {displayContent}
     </div>
   </div>
 );
}

export default nav;