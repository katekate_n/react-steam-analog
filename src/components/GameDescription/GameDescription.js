import React, {useState, useEffect} from 'react';
import { useLocation} from "react-router";
import './GameDescription.scss';
import { Link } from 'react-router-dom';


const GameDescription = (props) => {

   let game;

   const Init = () => {
      let location = useLocation();
      console.log(location);
      game = (location.state);
      console.log(game);

  };
  Init();


  const getCategories = game.categories.map( x => (x.description + " ▪ "));
  const getGenres = game.genres.map(x => (x.description + " ▪ "));

   const test = game.background;
   const sectionStyle = {
      backgroundImage: "url(" + test + ")"
   };

   return (
      <div className= "info" style={ sectionStyle }>
         <button className="home-btn">
            <Link to='/'>back</Link>
         </button>
         <div className= "info__title">{game.name}</div>
         <div className= "info__desc">
            <img className= "info__img" src= {game.header_image} />
            <div className= "info__text">{game.short_description}</div>
         </div>
         <div className= "info__details">
            <span className= "info__details--bold">Developer:</span>{game.developers}
         </div>
         <div className= "info__details">
            <span className= "info__details--bold">Publisher:</span>{game.publishers}
         </div>
         <div className= "info__details">
            <span className= "info__details--bold">Genre:</span>{getGenres}
         </div>
         <div className= "info__details">
            <span className= "info__details--bold">Category:</span>{getCategories}
         </div>
         <div className= "info__details">
            <span className= "info__details--bold">Release date:</span>{game.release_date.date}
         </div>

         { game.is_free || game.price_overview == null 
                  ? ( <div className= "info__details">
                        <span className= "info__details--bold">Price:</span> free
                     </div> )
                  : ( <div className= "info__details">
                        <span className= "info__details--bold">Price:</span> {game.price_overview.final / 100} {game.price_overview.currency}
                     </div> )}
         <div className= "info__details">
            <span className= "info__details--bold">Userscore:</span>{game.userscore}
         </div>
         <div className= "info__details">
            <span className= "info__details--bold">Owners:</span>{game.owners}
         </div>
      </div>
   )
}

export default GameDescription;