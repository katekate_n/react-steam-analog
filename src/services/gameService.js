

const GameService = {

   getAll : async function() {

      //const data = await fetch('https://73653f6a.ngrok.io/api/steam');
      //const data = await fetch('https://f6a32399.ngrok.io/api/steam', {cache: "force-cache"});
      const data = await fetch('https://steamapi20200226020656.azurewebsites.net/api/steam');
      const items = await data.json();

      //console.log(items);
      return items;
   },

   getTrending: async function () {
      let all = await this.getAll(); //todo

      let trending = all.sort((a, b) => {
         //ccu - concurrent player for yesterday 
         return b.ccu - a.ccu;
      }).slice(0, 100);

      trending.forEach(function (item, i) { item.index = i + 1; });

      return trending;
   },

   getUpcomming: async function () {
      let all = await this.getAll(); //todo
      let upcomming = all.filter(x=> x.release_date.coming_soon && x.release_date.date !=null );
      upcomming = upcomming.sort((a, b) => {
         return new Date(a.release_date.date) - new Date(b.release_date.date);
      }).slice(0, 15);
      
      return upcomming;
   },

   getRecentGames: async function () {
      let all = await this.getAll(); //todo
      let recent = all.filter(x=> x.release_date.coming_soon == false && x.release_date.date !=null );
      recent = recent.sort((a, b) => {
         return new Date(b.release_date.date) - new Date(a.release_date.date);
      }).slice(0, 100);
      

      recent.forEach(function (item, i) { item.index = i + 1; });

      return recent;
   }

}

export default GameService; 