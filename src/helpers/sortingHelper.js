const SortingHelper =  {

   sortResultsByState: function(sortingState, results) {
      if (sortingState.reverse != null) {
         results = sortingState.reverse 
         ? results.reverse() 
         : results;
      }

      if (sortingState.date != null) {
         results =  results.sort(function (a, b) {
            let c = new Date(a.release_date.date);
            let d = new Date(b.release_date.date);

            //якщо вважаєм шо а == б рівні 
            if(a.release_date.coming_soon == true && b.release_date.coming_soon == true)
               return 0;
            //  якщо а менше б
            if(a.release_date.coming_soon == true && b.release_date.coming_soon == false)
               return sortingState.date ? -1 : 1;

            // якщо а більше б
            if (a.release_date.coming_soon == false && b.release_date.coming_soon == true)
               return sortingState.date ? 1 : -1;

            return sortingState.date ? c - d : d - c;
         }); 
      }

      if (sortingState.owners != null) {
         results = results.sort(function (a, b) {
            var aTest = parseFloat(a.owners.slice(a.owners.lastIndexOf('.') + 1, a.owners.length));
            var bTest = parseFloat(b.owners.slice(b.owners.lastIndexOf('.') + 1, b.owners.length));

            return sortingState.owners ? aTest - bTest : bTest - aTest;
         })
      }

      // if (sortingState.price != null) {
      //    results = sortingState.price 
      //    ? results.sort((a, b) => {
      //       return parseFloat(a.price.isfree ? 0 : ) - parseFloat(b.price);
      //    }) 
      //    : results.sort((a, b) => {
      //          return parseFloat(b.price) - parseFloat(a.price);
      //       });
      // }


      //if(a.isree == true && b.release_date.coming_soon == true)
      // return 0;

      if (sortingState.price != null) {
         results =  results.sort(function (a, b) {

            if(a.price_overview != null && b.price_overview != null ){
               let c = a.price_overview.final;
               let d = b.price_overview.final;

               return sortingState.price ? c - d : d - c;
            }

            if(a.is_free == true && b.is_free == true)
               return 0;

            if(a.is_free == true && b.is_free == false) //free is less than not free
               return sortingState.price ? -1 : 1; 

            if (a.is_free == false && b.is_free == true)
               return sortingState.price ? 1 : -1;

         });
      }

      if (sortingState.reviews != null) {
         results = results.sort(function (a, b) {
         //   return a.positive - b.positive;
         return sortingState.reviews ? a.positive - b.positive : b.positive - a.positive;

         })
      }

      if (sortingState.name != null) {
         results = results.sort(function (a, b) {
            if (a.name < b.name) { return sortingState.name ? -1 : 1 }
            if (a.name > b.name) { return sortingState.name ? 1 : -1 }
            return 0;
         })
      }

      return results;
   }
}

export default SortingHelper;