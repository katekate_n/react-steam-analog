import React from 'react';
import './App.scss';
import Nav from '../components/Nav/Nav';
import GameDescription from '../components/GameDescription/GameDescription';
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

function App() {
    return (
      <div className = "App">
          <Router>
            <Route path= "/" exact component={Nav} />
            <Route path= "/game" component= {GameDescription} />
          </Router>
      </div>
    );
}

export default App;